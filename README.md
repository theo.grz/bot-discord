# BotTest

Ce bot Discord permet aux utilisateurs de jouer à un jeu de devinettes de mots et de rechercher des résumés d'articles Wikipedia directement dans Discord. Le jeu choisit aléatoirement un mot depuis Wikipedia et challenge les joueurs à le deviner sur la base des lettres correctement placées.

## Fonctionnalités

- **Recherche Wikipedia** : Les utilisateurs peuvent rechercher des résumés d'articles Wikipedia.
- **Jeu de Devinettes de Mots** : Un jeu interactif où les joueurs doivent deviner un mot choisi aléatoirement par le bot.
- **Commandes Utilisateur** : Commandes pour interagir avec le bot, y compris saluer, tester, demander de l’aide, et plus.

### Commandes

- `!hello` : Saluer le bot.
- `!test` : Tester le bot pour s'assurer qu'il fonctionne.
- `!help` : Afficher un message d'aide détaillant les commandes disponibles.
- `!m [mot]` : Jouer au jeu de devinettes en soumettant un mot.
- `!relance` : Commencer un nouveau jeu en choisissant un nouveau mot.
- `!w [terme]` : Rechercher un résumé d'article Wikipedia pour un terme spécifié.

## Installation et Exécution

### Prérequis

Avoir Python installé sur votre machine. Ce bot nécessite `discord.py` et `requests`.

### Configuration du bot sur Discord

Créez un bot via le [Portail des développeurs Discord](https://discord.com/developers/applications) et notez le token du bot.

### Configurer le token du bot dans le script

Ouvrez le script du bot et remplacez `'VOTRE_TOKEN_DISCORD'` par le token de votre bot à la ligne 122.

## Note

J'ai aimé réaliser ce bot Discord en Python. Ce cours est une bonne façon d'aborder Python en laissant de la créativité aux étudiants. Je voulais développer un Wikipedia racing, mais je n'ai pas réussi à être satisfait de mon travail, donc je ne l'ai pas implémenté. J'ai également eu des difficultés à installer `discord.py` sur ma machine, à part cela, je n'ai pas eu de problème.
