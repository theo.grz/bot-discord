# Importation des modules nécessaires
import discord
import requests
import random
import re

# Définition de la classe MyClient, qui hérite de discord.Client
class MyClient(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Initialisation des variables pour stocker le mot cible et les lettres découvertes
        self.target_word = None
        self.discovered_letters = []

    # Méthode appelée lorsque le bot est prêt et connecté
    async def on_ready(self):
        print(f'Logged on as {self.user}!')

    # Méthode pour choisir un mot aléatoire depuis Wikipedia
    async def choose_random_word_from_wikipedia(self):
        # Configuration de l'URL et des paramètres pour l'API Wikipedia
        url = "https://fr.wikipedia.org/w/api.php"
        params = {
            "action": "query",
            "format": "json",
            "list": "random",
            "rnnamespace": 0,
            "rnlimit": 10
        }
        # Requête HTTP pour obtenir des titres aléatoires
        response = requests.get(url, params=params)
        data = response.json()

        # Traitement de la réponse pour extraire un mot aléatoire
        if 'query' in data and 'random' in data['query']:
            titles = [page['title'] for page in data['query']['random']]
            words = [re.findall(r'\b[a-zA-Z]{3,}\b', title) for title in titles]
            words = [word.lower() for sublist in words for word in sublist if word.isalpha()]

            if words:
                self.target_word = random.choice(words)
                self.discovered_letters = ['*'] * len(self.target_word)
            else:
                self.target_word = 'erreur'
                self.discovered_letters = ['*'] * len(self.target_word)
        else:
            self.target_word = 'erreur'
            self.discovered_letters = ['*'] * len(self.target_word)

        print(f"Mot choisi: {self.target_word}")

    # Méthode pour mettre à jour les lettres découvertes
    def update_discovered_letters(self, user_word):
        response = ""
        for i in range(len(self.target_word)):
            if i < len(user_word) and user_word[i] == self.target_word[i]:
                response += self.target_word[i]
            else:
                response += self.discovered_letters[i]
        self.discovered_letters = list(response)
        return response

    # Gestion des messages reçus
    async def on_message(self, message):
        # Ignorer les messages du bot lui-même
        if message.author == self.user:
            return

        # Commandes supportées par le bot
        if message.content.startswith('!hello'):
            await message.channel.send(f'Hello {message.author}!')

        elif message.content.startswith('!test'):
            await message.channel.send('testOk')

        elif message.content.startswith('!help'):
            help_text = ("Commandes disponibles :\n"
                         "!hello - Saluer le bot\n"
                         "!test - Tester le bot\n"
                         "!help - Afficher ce message d'aide\n"
                         "!m [mot] - Jouer au jeu de devinette de mots\n"
                         "!relance - Choisir un nouveau mot pour le jeu\n"
                         "!w [terme] - Rechercher un terme sur Wikipedia et afficher les informations")
            await message.channel.send(help_text)

        # Recherche sur Wikipedia
        elif message.content.startswith('!w'):
            parts = message.content.split(' ')
            if len(parts) >= 2:
                search_term = ' '.join(parts[1:])
                # Requête pour rechercher un terme sur Wikipedia
                search_url = f"https://fr.wikipedia.org/w/api.php?action=query&list=search&srsearch={search_term}&format=json"
                
                search_response = requests.get(search_url)
                search_data = search_response.json()

                if search_data['query']['search']:
                    title = search_data['query']['search'][0]['title']
                    page_url = f"https://fr.wikipedia.org/wiki/{title.replace(' ', '_')}"
                    summary_url = f"https://fr.wikipedia.org/api/rest_v1/page/summary/{title.replace(' ', '_')}"
                    summary_response = requests.get(summary_url)
                    summary_data = summary_response.json()

                    message_text = f"**{title}**\n"
                    if 'extract' in summary_data and summary_data['extract'].strip() != '':
                        message_text += summary_data['extract']
                    message_text += f"\nPlus d'informations: {page_url}"
                    await message.channel.send(message_text)
                else:
                    await message.channel.send(f"Aucun résultat trouvé pour {search_term}.")

        # Jeu de devinettes de mots
        elif message.content.startswith('!m '):
            if self.target_word is None:
                await self.choose_random_word_from_wikipedia()

            parts = message.content.split(' ')
            if len(parts) >= 2:
                user_word = parts[1]
                response = self.update_discovered_letters(user_word)

                if response == self.target_word:
                    await message.channel.send(f"Félicitations ! Vous avez trouvé le mot : {self.target_word}")
                    await self.choose_random_word_from_wikipedia()
                else:
                    await message.channel.send(f"`{response}`")

        # Relancer le jeu
        elif message.content.startswith('!relance'):
            await self.choose_random_word_from_wikipedia()
            await message.channel.send("Nouveau mot choisi, vous pouvez recommencer à jouer avec !m")

# Configuration des intents nécessaires pour le bot
intents = discord.Intents.default()
intents.messages = True
intents.message_content = True

# Création et exécution du bot avec le token spécifié
client = MyClient(intents=intents)
client.run('VOTRE_TOKEN_DISCORD')
